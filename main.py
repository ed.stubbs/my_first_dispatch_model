import gdxpds
import pandas as pd
import numpy as np
import scipy as sp
import pprint as pp
import matplotlib.pyplot as plt

# plt.style.use('ggplot')


def function():
    power = gdxpds.to_dataframes('outputs.gdx')['output']
    demand = gdxpds.to_dataframes('outputs.gdx')['demandp']
    charge = gdxpds.to_dataframes('outputs.gdx')['charge']

    power['t'] = power['t'].astype(int)
    demand['t'] = demand['t'].astype(int)
    charge['t'] = charge['t'].astype(int)

    power_data = power.pivot(index='t', columns='source', values='Level')

    shadow_price = demand[['Level', 'Marginal']]
    shadow_price = shadow_price.set_index('Level')
    shadow_price = shadow_price.sort_index()
    shadow_price = shadow_price.reset_index()

    charge_data = charge.pivot(index='t', columns='batt', values='Level')
    charge_data = charge_data.reset_index()

    shadow_price.plot.line(x='Level', y='Marginal', label='shadow price (EUR / MWh)')

    ax = power_data.plot(stacked=True, kind='bar')
    demand[['Level']].plot.line(linestyle='', marker='_', color='tab:red', ax=ax, label='demand (MWh)')
    ax.grid(linestyle='--')
    ax2 = ax.twinx()
    charge_data[['store']].plot.line(linestyle='-', marker='o', color='tab:purple', ax=ax2, label='MWh stored')
    ax2.get_legend().remove()

    ax.set_ylabel('Generation (MWh)')
    ax2.set_ylabel('Stored energy (MWh)')

    h1, l1 = ax.get_legend_handles_labels()
    if l1[0] == 'Level':
        l1[0] = 'Demand'
    h2, l2 = ax2.get_legend_handles_labels()
    ax.legend(h1+h2, l1+l2, loc=0)
    plt.show()
    return


if __name__ == "__main__":
    function()
